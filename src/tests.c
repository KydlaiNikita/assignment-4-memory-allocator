#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#define page 1024

static void clear_heap(void *heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity){size}).bytes);
}

static void print_test(int test_id) {
    fprintf(stdout, "TEST %i", test_id);
}

static void print_success(int test_id) {
    fprintf(stdout, "TEST %i passed", test_id);
}

static void print_failure(int test_id) {
    fprintf(stderr, "TEST %i failed", test_id);
}


void malloc_test() {
    print_test(1);
    struct block_header* heap = (struct block_header*) heap_init(REGION_MIN_SIZE);
    if (!heap) {
        print_failure(1);
        return;
    }
    debug_heap(stdout, heap);
    void *alloc = _malloc(page);
    debug_heap(stdout, heap);
    if (heap->is_free || !alloc) {
        print_failure(1);
        clear_heap(heap, REGION_MIN_SIZE);
        return;
    }

    _free(alloc);
    clear_heap(heap, REGION_MIN_SIZE);
    print_success(1);
}

void free_one_block_test() {
    print_test(2);
    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void *first = _malloc(page);
    void *second = _malloc(page);
    debug_heap(stdout, heap);
    _free(first);
    if (!second) {
        print_failure(2);
        clear_heap(heap, REGION_MIN_SIZE);
        return;
    }
    debug_heap(stdout, heap);
    _free(second);
    clear_heap(heap, REGION_MIN_SIZE);
    print_success(2);
    }

void free_two_blocks_test() {
    print_test(3);
    struct block_header* heap = (struct block_header*) heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void *first = _malloc(page);
    void *second = _malloc(page);
    void *third = _malloc(page);
    debug_heap(stdout, heap);
    _free(first);
    _free(third);
    if (!second) {
        print_failure(3);
        clear_heap(heap, REGION_MIN_SIZE);
        return;
    }
    debug_heap(stdout, heap);
    _free(second);
    clear_heap(heap, REGION_MIN_SIZE);
    print_success(3);
}

void extend_old_region_test() {
    print_test(4);
    struct block_header* heap = (struct block_header*) heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void *alloc = _malloc(REGION_MIN_SIZE + 1);
    if (size_from_capacity(heap->capacity).bytes < 1 + REGION_MIN_SIZE) {
        print_failure(4);
        _free(alloc);
        clear_heap(heap, REGION_MIN_SIZE + 1);
        return;
    }
    debug_heap(stdout, heap);
    _free(alloc);
    clear_heap(heap, REGION_MIN_SIZE + 1);
    print_success(4);
    }

void not_extend_old_region_test() {
    print_test(5);
    struct block_header* heap = (struct block_header*) heap_init(REGION_MIN_SIZE);
    if (!heap) {
        print_failure(5);
        return;
    }
    debug_heap(stdout, heap);
    void *first = _malloc(REGION_MIN_SIZE);
    if (!first) {
        print_failure(5);
        return;
    }
    debug_heap(stdout, heap);
    struct block_header* block = first - offsetof(struct block_header, contents);
    void* addr = block->contents + block->capacity.bytes;
    void* tmp = mmap(addr,REGION_MIN_SIZE,PROT_READ | PROT_WRITE,
                      MAP_PRIVATE | MAP_FIXED,-1,0);
    void *second = _malloc(REGION_MIN_SIZE);
    if (!second) {
        print_failure(5);
        _free(first);
        clear_heap(heap, REGION_MIN_SIZE);
        clear_heap(tmp, REGION_MIN_SIZE);
        return;
    } else if (heap == tmp) {
        print_failure(5);
        _free(first);
        _free(second);
        clear_heap(heap, REGION_MIN_SIZE);
        clear_heap(tmp, REGION_MIN_SIZE);
        return;
    }
    debug_heap(stdout, heap);
    _free(first);
    _free(second);
    clear_heap(heap, REGION_MIN_SIZE);
    clear_heap(tmp, REGION_MIN_SIZE);
    print_success(5);
    }
